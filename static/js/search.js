let search_url = "/api/search_box/"
const searchInput = document.getElementById('search-input');

function createListGroupItem(username, id) {
    try{
        if (following.includes(id)){
            color = "btn btn-dark"
            word = "unfollow"
        }
        else {
            color = "btn btn-primary"
            word = "follow"
        }
    }catch{
        color = "btn btn-primary"
        word = "follow"
    }
    
    return `
      <li class="list-group-item d-flex justify-content-between align-items-start" style="height: 100px;">
        <div class="ms-2 me-auto">
          <div class="fw-bold">${username}</div>
        </div>
        <span class="badge ${color} rounded-pill" onclick="followFunc(event.target.attributes, event.target)" id="followid_${id}" value="${id}" >&nbsp${word}&nbsp</span>
      </li>
    `;
}


searchInput.addEventListener('keyup', function (event) {
    document.querySelector('#wrap-content').style.display = 'none'
    document.querySelector('.main-search-card').style.display = 'flex'
    if (document.querySelector('#hashtag-box')) document.querySelector('#hashtag-box').style.display = 'none'
    reply = document.getElementById("shit-happened")
    if (reply) reply.style.display = "none"
    const searchTerm = event.target.value;
    fetch(search_url, {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({ search_field: searchTerm }),
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken
        }
    }).then(response => {
        return response.json();
    }).then(data => {
        let userElement;
        let tweetElement;
        let arr = data.data.related_users
        let relatedUsersElement = document.getElementById('related_user_append_id');
        let relatedTweetElement = document.getElementById('related_tweets');
        relatedUsersElement.innerHTML = ''
        relatedTweetElement.innerHTML = ''
        for (let index = 0; index < arr.length; index++) {
            userElement = createListGroupItem(arr[index].username, arr[index].id)
            relatedUsersElement.innerHTML += userElement
        }
        arr = data.data.related_tweets
        for (let index = 0; index < arr.length; index++) {
            const elementArr = arr[index]
            userElement = {
                key: elementArr.tweet_id,
                author: elementArr.author_username,
                comments: elementArr.num_comments,
                likes: elementArr.num_likes,
                text: elementArr.text,
                liked_users: elementArr.liked_users,
                bookmarked_users: elementArr.bookmarked_users,
            }
            tweetElement = commentElement(userElement, "tweet")
            relatedTweetElement.appendChild(tweetElement)
        }
    }).catch(error => {
        console.error(error);
    });
});


function searchTab(data) {
    const related_tweets = document.getElementById('related_tweets')
    const related_users = document.getElementById('related_user_append_id')
    if (data == "Tweets") {
        related_tweets.style.display = 'none'
        related_users.style.display = ''

    } else {
        related_users.style.display = 'none'
        related_tweets.style.display = ''
    }
}








function search_trigger() {
    searchInput.dispatchEvent(new Event('keyup'));
}



function getTagTweet(id) {
    document.getElementById('wrap-content').style.display = 'none'
    document.querySelector('.main-search-card').style.display = 'none'
    reply = document.getElementById("shit-happened")
    if (reply) reply.remove()
    document.getElementById('spinner').style.opacity = 0.5
    fetch("/api/get_tags_tweet/?hashtag_id=" + id, {
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken
        }
    }).then(response => {
        return response.json()
    }).then(data => {
        setTimeout(function () {
            const element = document.getElementById("hashtag-box");
            if (element) element.remove();
            const newDiv = document.createElement("div");
            newDiv.setAttribute('id', 'hashtag-box')
            arr = data.related_tweets
            for (let index = 0; index < arr.length; index++) {
                const elementArr = arr[index]
                userElement = {
                    key: elementArr.tweet_id,
                    author: elementArr.author_username,
                    comments: elementArr.num_comments,
                    likes: elementArr.num_likes,
                    text: elementArr.text,
                    liked_users: elementArr.liked_users,
                    bookmarked_users: elementArr.bookmarked_users,
                }
                tweetElement = commentElement(userElement, "tweet").outerHTML
                newDiv.innerHTML += tweetElement

            }
            myDiv = document.querySelector('.main')
            myDiv.appendChild(newDiv);
            document.getElementById('spinner').style.opacity = 0
        }, 2000)

    })
}


function followFunc(elementAttr,element){

    let following_id = elementAttr.getNamedItem("value").value
    let btnIdentifier = elementAttr.getNamedItem("class").value
    let state;
    const str = btnIdentifier;
    if (str.includes("primary")) {
        btnIdentifier="badge btn btn-dark rounded-pill"
        state = "follow"
    } else {
        state="unfollow"
        btnIdentifier = "badge btn btn-primary rounded-pill"
    }
    fetch("/api/user_follow/", {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            following_id:following_id,
            state : state
        }),
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': csrfToken
        }
      }).then(response => {

        if(response.status ==401){
            Swal.fire({
                title: 'Please login to follow/unfollow',
                text: '',
                icon: 'warning',
                confirmButtonText: 'OK'
            });
            return;
        }
        state=="follow" ? state= "unfollow" : state ="follow"
        element.setAttribute('class',`${btnIdentifier}`)
        element.textContent = state
      });
}