let stack = [1];
let reply_url = "/api/reply/?tweet_id="
let createTweetUrl = "/api/tweet_detail/?action=create_tweet"
let PrevElement = ''

function getComments(element, id, parend_id = null) {
  PrevElement = document.querySelector(`#${element}`)
  TweetID = id
  ParentID = parend_id
  document.getElementById("wrap-content").innerHTML=''
  document.getElementById("wrap-content").style.display='none'
  document.getElementsByClassName("main-search-card")[0].style.display = 'none';
  if (document.querySelector('#hashtag-box')) document.querySelector('#hashtag-box').style.display = 'none'
  fetch(`/api/reply/?tweet_id=${id}${parend_id ? "&parent_reply=" + parend_id : ''}`, {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrfToken
    }
  }).then(response => { 
    return response.json() 
  })
    .then(data => {
     
      document.getElementById('spinner').style.opacity = 0.5
      if (document.getElementById("shit-happened")) {
        let existingElement = document.getElementById(element);
        if (existingElement && existingElement.closest('#shit-happened')) {
          existingElement.remove();
        }
        document.getElementById("shit-happened").remove();
      }
      setTimeout(function () {
        let tabMain = document.querySelector('.main');
        let newDiv = document.createElement('div');
        newDiv.setAttribute("id", "shit-happened");
        newDiv.appendChild(PrevElement)
        newDiv.innerHTML += replyBox()
        for (let index = 0; index < data.length; index++) {
          let elementArr = data[index]
          if (elementArr.comments) {
            tweetElement = commentElements(elementArr, "reply_", elementArr.id)
            tweetElement.style.paddingLeft = '40px'
            for (let i = 0; i < elementArr.comments.length; i++) {
              let commentElementArr = elementArr.comments[i]
              let commentElement = commentElements(commentElementArr, "reply_", commentElementArr.id)
              commentElement.style.padding = '50px'
              commentElement.style.paddingLeft = '60px'
              tweetElement.appendChild(commentElement)
            }
          } else {
            tweetElement = commentElements(elementArr, "reply_", elementArr.id)
          }
          newDiv.innerHTML += tweetElement.outerHTML;
        }

        tabMain.appendChild(newDiv);
        document.getElementById('spinner').style.opacity = 0
      }, 1000)

    })
}




function commentElements(tweet, state, parent_id = null) {
  let tweetElement = document.createElement('div');
  tweetElement.setAttribute('class', 'comment');
  tweetElement.setAttribute('id', "reply_" + tweet.id);
  tweetElement.setAttribute('style', 'padding-left:10px');
  let tweetAuthor = tweet.author__name.trim();
  requestUser = requestUser.trim();
  let placeholder;
  tweetAuthor === requestUser ? placeholder ="x" : placeholder = ''
  tweetElement.innerHTML = `
      <div class="tweet-header">
        <div class="tweet-header-info">
          <h4 class="tweet-name">${tweet.author__name}</h4>
          <p class="tweet-username">@${tweet.author__name}</p>
          <p class="tweet-username" id="tweet_time_${tweet.id}"></p>
        </div>
        <p class="tweet-delete" onclick="deleteElement(${tweet.id})">${placeholder}</p>
      </div>
      <p class="tweet-text" onclick="getComments('reply_${tweet.id}','${tweet.tweet_id}','${parent_id}')">${tweet.text}</p>
      <div class="tweet-icons">
        <div class="comment-icon" onclick='replyFunc("${tweet.id}","${state}")'>
          <i class="fa fa-comment tweet-small-icon"></i>
          <span class="comment-count">${tweet.num_comments}</span>
        </div>
        <div class="heart-icon">
          <i class="fa fa-heart likes tweet-small-icon" reply_id="${tweet.id}" onclick="likeFunc(event.target.attributes, event.target,'reply')"
            style='color:${tweet.liked_users == "true" ? "red" : ""}'
            value="${String(tweet.liked_users) == "true" ? "true" : "false"}" tweet_id="${tweet.key}"></i>
          <span class="heart-count" id="tweetlikespan_${tweet.id}">${tweet.num_likes}</span>
        </div>
        <div class="bookmark-icon">
          <i class="fa fa-bookmark bookmark tweet-small-icon" onclick="bookmark(event.target.attributes, event.target);"
            value="${tweet.bookmarked_users == "true" ? "true" : "false"}"  tweet_id="undefined" reply_id="${tweet.id}"
            style='color:${String(tweet.bookmarked_users) == "true" ? "red" : ""}'></i>
        </div>
      </div>
    `;
  return tweetElement;
}

function navTab() {
  return `<div class="nav-tab">
  <img src="https://img.icons8.com/ios-filled/50/null/long-arrow-left.png" />
</div> <hr>`
}

function replyBox() {
  return ` <div class="tweet-box" id="replychain">
  <textarea placeholder="Reply Here!...." rows="2" id="comment-val" ></textarea>
  <div class="actions">
      <button type="button" onclick="Reply()">Reply</button>
  </div>
</div>`
}




function commentElement(tweet, state) {
  let tweetElement = document.createElement('div');
  tweetElement.setAttribute('class', 'tweet');
  tweetElement.setAttribute('id', "reply_" + tweet.key);
  tweetElement.setAttribute('value', tweet.key);
  let tweetAuthor = tweet.author.trim();
  requestUser = requestUser.trim();
  let placeholder;
  tweetAuthor === requestUser ? placeholder ="x" : placeholder = ''
  tweetElement.innerHTML = `
      <div class="tweet-header">
        <div class="tweet-header-info">
          <h4 class="tweet-name">${tweet.author}</h4>
          <p class="tweet-username">@${tweet.author}</p>
          <p class="tweet-username" id="tweet_time_${tweet.key}"></p>
        </div>
        <p class="tweet-delete" onclick="deleteElement(${tweet.key})">${placeholder}</p>
      </div>
      <p class="tweet-text" onclick="getComments('reply_${tweet.key}','${tweet.key}')">${tweet.text}</p>
      <div class="tweet-icons">
        <div class="comment-icon" onclick='replyFunc("${tweet.key}")'>
          <i class="fa fa-comment tweet-small-icon"></i>
          <span class="comment-count">${tweet.comments}</span>
        </div>
        <div class="heart-icon">
          <i class="fa fa-heart likes tweet-small-icon"   onclick="likeFunc(event.target.attributes, event.target,'tweet')"
            style='color:${String(tweet.liked_users) == "true" ? "red" : ""}'
            value="${String(tweet.liked_users) == "true" ? "true" : "false"}" tweet_id="${tweet.key}"></i>
          <span class="heart-count" id="tweetlikespan_${tweet.key}">${tweet.likes}</span>
        </div>
        <div class="bookmark-icon">
          <i class="fa fa-bookmark bookmark tweet-small-icon" onclick="bookmark(event.target.attributes, event.target);"
            value="${String(tweet.bookmarked_users) == "true" ? "true" : "false"}" tweet_id="${tweet.key}"
            style='color:${String(tweet.bookmarked_users) == "true" ? "red" : ""}'></i>
        </div>
      </div>
    `;
  return tweetElement;
}


function postTweet(e) {

  if (currentuser == "False") {
    Swal.fire({
      title: 'Please login to post!',
      text: '',
      icon: 'info',
      confirmButtonText: 'OK'
    });
    document.querySelector(".tweet-box textarea").value = ''
    return;
  }
  e.preventDefault()
  let text = document.querySelector(".tweet-box textarea").value
  fetch(createTweetUrl, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify({ text: text }),
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrfToken
    }
  }).then(response => {
    return response.json()
  }).then(data => {
    console.log(data)
    const myDiv = document.querySelector('.tweet-box');
    myDiv.insertAdjacentHTML('afterend', commentElement(data.data, "tweet_").outerHTML);
    timestampConvert(data.data.time, "tweet_time_" + data.data.key)
    document.querySelector('.tweet-box textarea').value = ""
  })
}



function logout() {
  fetch("/logout/", {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrfToken
    }
  }).then(response => {
    window.location.href = "/index"
  });
}


function deleteElement(id) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'This action cannot be undone.',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No'
  }).then((result) => {
    if (result.isConfirmed) {
      fetch("/api/tweet_detail/?action=delete_tweet", {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({ tweet_id: id }),
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': csrfToken
        }
      }).then(res=>res.json()).then(response => {
        console.log(response)
        if(document.getElementById("tweet_" + id)) document.getElementById("tweet_" + id).remove()
        if(document.getElementById("reply_" + id)) document.getElementById("reply_" + id).remove()
      });
    }
  })

}

function Reply() {
  fetch("/api/reply/", {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify({ tweet: TweetID, parent_reply: ParentID, text: document.getElementById('comment-val').value }),
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrfToken
    }
  }).then(response => {
    if(response.status == 401) return []
    return response.json()
  }).then(data => {
    if(data.length==0){
      Swal.fire({
        title: 'Please login to comment!',
        text: '',
        icon: 'info',
        confirmButtonText: 'OK'
      });
      document.getElementById('comment-val').value = ''
      return;
    }
    data = data.data
    data = {
      id: data.id,
      key: data.tweet,
      author__name: data.author_name,
      num_comments: 0,
      num_likes: 0,
      text: data.text,
      liked_users: data.false,
      bookmarked_users: false,
    }
    const replychainDiv = document.getElementById("replychain");
    elementToAdd= commentElements(data, 'reply_', data.id)
    elementToAdd.style.marginLeft='15px'
    replychainDiv.insertAdjacentElement('beforebegin',elementToAdd);
    document.getElementById('comment-val').value = ''
  })
}

