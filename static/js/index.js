let page = 2
let bookmark_url = "/api/tweet_detail/?action=tweet_bookmark"
let like_url = "/api/tweet_detail/?action=like"
let getTweets_url = `/api/get_tweets/?page=`
let TweetID = ''
let ParentID = ''




function bookmark(element, clickedElement) {

    let tweet_id = element.getNamedItem("tweet_id").value;
    action_for = "tweet"
    // alert(tweet_id)
    if(tweet_id == "undefined") {
        
        tweet_id = element.getNamedItem("reply_id").value
        action_for = "reply"
    } 
   
    let bookmark = element.getNamedItem("value").value;
    let action;
    bookmark == 'true' ? action = "remove" : action = "add"
    const data = {
        action: action,
        tweet_id: tweet_id,
        action_for:action_for
    }
 
    fetch(bookmark_url, {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken
        }
    }).then(response => {
        if (response.status == 200) {
            let color;
            let state;
            bookmark == 'true' ? state = "false" : state = "true"
            bookmark == 'true' ? color = "grey" : color = "red"
            
            clickedElement.style.color = color
            clickedElement.setAttribute("value", state);
        }
        else if (response.status == 401) {
            Swal.fire({
                title: 'Please login!',
                text: '',
                icon: 'warning',
                confirmButtonText: 'OK'
            });
        }

    });
}





function likeFunc(element, clickedElement, action_for) {
    let id_of;
    id_of = element.getNamedItem("tweet_id").value;
    if(id_of == "undefined") {
        id_of = element.getNamedItem("reply_id").value
    } 
    let like = element.getNamedItem("value").value;
    let action;
    like == 'true' ? action = "remove" : action = "add"
    const data = {
        action: action,
        id_of: id_of,
        action_for: action_for
    }

    fetch(like_url, {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken
        }
    })
        .then(response => {
            if (response.status == 401) {
                Swal.fire({
                    title: 'Please login!',
                    text: '',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                });
            } else {
                let color;
                let state;
                let innerCount = document.getElementById(`tweetlikespan_${id_of}`).textContent
                if(!innerCount) innerCount = 0
                if (like == "true") {
                    state = "false"
                    color = "grey"
                    innerCount = parseInt(innerCount) - 1
                } else {
                    state = "true"
                    color = "red"
                    innerCount = parseInt(innerCount) + 1
                }
                let elements = document.querySelectorAll(`[id="tweetlikespan_${id_of}"]`);
                for (let i = 0; i < elements.length; i++) {
                        elements[i].textContent = innerCount;
                }
                clickedElement.style.color = color
                clickedElement.setAttribute("value", state);
            }
        });
}




window.addEventListener('scroll', () => {
    const { scrollTop, scrollHeight, clientHeight } = document.documentElement;

    if (scrollTop + clientHeight >= scrollHeight) {
        document.getElementById('spinner').style.opacity = 0.5
        setTimeout(function () {
            loadMoreContent();
        }, 1000)
    }
});

async function loadMoreContent() {
    
    await fetch(`${getTweets_url + page}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken
        }
    }).then(response => response.json())
        .then(data => {
            // if(!data) return;
            let list = document.getElementById("wrap-content");
            obj = Object.keys(data)
            for (let index = 0; index < obj.length; index++) {
                list.appendChild(commentElement(data[obj[index]], "tweet_"));
                timestampConvert(data[obj[index]].time, "tweet_time_" + data[obj[index]].key)
            }
            page += 1
        });
    document.getElementById('spinner').style.opacity = 0
}

