from django.apps import AppConfig


class TwitterMomodelConfig(AppConfig):
    name = 'api'

    def ready(self):
        import api.signals
