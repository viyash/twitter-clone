from django.db.models.signals import post_save
from django.dispatch import receiver
from api.models import User, UserRelationship
import threading


@receiver(post_save, sender=User)
def my_handler(sender,instance, created,**kwargs):
    print('started - ', instance.id)
    UserRelationship.objects.get_or_create(user_id=instance)
    print('completed - ', instance.id)

    
   




