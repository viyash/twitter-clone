from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status


from django.db.models import Count, Q, Prefetch, F
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from .models import Reply, User, UserRelationship, Tweet, TweetHashtag, Hashtag, UserTweetLikes
from .serializer import (
    ReplySerializer,
    Reply2Serializer,
    TweetSerializer,
    TrendingTagsSerializer,
    UserSerializer,
    UserRelationshipSerializer
)
from django.contrib.contenttypes.models import ContentType



import random
import json
from summa.keywords import keywords
from datetime import datetime, timedelta



def get_nested_reply(tweet_id,parent_reply):
    reply_query = reply_query = Reply.objects.filter(tweet__tweet_id=tweet_id, parent_reply=parent_reply).annotate(
                num_likes=Count('likes', distinct=True),
                num_comments=Count('parent_replies', distinct=True),
            ).values(
            'id','text', 'author__name', 'tweet_id', 'num_comments', 'num_likes', 'time', 'author__id', 'bookmarked_users','liked_users'
    )
    return reply_query


@api_view(['GET', 'POST'])
def reply(request):
    response = {"msg": "", "data": {}}
    try:
        if request.method == 'GET':
            tweet_id = request.GET.get('tweet_id')
            parent_reply = request.GET.get('parent_reply')
            reply_query = Reply.objects.filter(tweet__tweet_id=tweet_id, parent_reply=parent_reply).annotate(
                num_likes=Count('likes', distinct=True),
                num_comments=Count('parent_replies', distinct=True),
            ).values(
            'id','text', 'author__name', 'tweet_id', 'num_comments', 'num_likes', 'time', 'author__id', 'bookmarked_users','liked_users'
            )
            serialized_reply = ReplySerializer(reply_query, many=True, context={'request': request}).data
            for obj in serialized_reply:
                if obj['num_comments'] == 1:
                    queryset = get_nested_reply(tweet_id, obj['id'])
                    obj['comments'] = ReplySerializer(queryset, many=True, context={'request': request}).data
            response['msg'] = "success"
            response['data'] = serialized_reply
            return Response(serialized_reply, status=status.HTTP_200_OK)
        elif request.method == 'POST':
            if not request.user.is_authenticated:
                response['msg'] = "User not authenticated"
                return Response(response, status=status.HTTP_401_UNAUTHORIZED)
            else:
                user_id = request.user.id
                tweet_id = request.data.get('tweet')
                text = request.data.get('text')
                parent_reply = request.data.get('parent_reply')
                parent_reply = Reply.objects.get(id=parent_reply) if parent_reply else None
                reply_obj = Reply.objects.create(
                    author_id=user_id,
                    tweet_id=tweet_id,
                    text=text,
                    parent_reply=parent_reply,
                )
                reply_obj.author_name=request.user.username
                response['data'] = Reply2Serializer(reply_obj, many=False).data
                response['msg'] = "success"
                return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET','POST'])
def get_tweets(request, page=1, per_page=5):
    print("hi")
    response = {"msg":""}
    try:
        my_dict = {}
        #pagination
        page = int(request.GET.get('page',1))
        offset = (page - 1) * per_page
        limit = per_page
        if not request.user.is_anonymous:
            from django.core.exceptions import ObjectDoesNotExist
            try:
                name = request.user.name
                users_query = UserRelationship.objects.get(user_id__name=name).following.all()
            except ObjectDoesNotExist:
                users_query = []
        else:
            queryset = tweets_for_anonymous(request, page, per_page)
            return Response(queryset, status = status.HTTP_200_OK)

        prefetch_likes = Prefetch('likes', queryset=UserTweetLikes.objects.filter(user__in=users_query))
        prefetch_replies = Prefetch('tweet_replies', queryset=Reply.objects.filter(parent_reply=None))
        tweets_query = Tweet.objects.filter(Q(author__in=users_query) | Q(author__name=name)).prefetch_related(
            prefetch_likes, prefetch_replies).annotate(
            num_comments=Count('tweet_replies', distinct=True ),#filter=Q(tweet_replies__parent_reply=None)
            num_likes=Count('likes', distinct=True),
        ).values(
            'text', 'author__name', 'tweet_id', 'num_comments', 'num_likes', 'time', 'author__id', 'bookmarked_users','liked_users'
        ).order_by('-time')[offset:offset + limit]
        for tweet in tweets_query:
            my_dict[tweet['tweet_id']] = {
                'text': tweet['text'],
                'key': tweet['tweet_id'],
                'likes': tweet['num_likes'],
                'comments': tweet['num_comments'],
                'author': tweet['author__name'],
                'author_id': tweet['author__id'],
                'time': tweet['time'],
                'liked_users': "true" if request.user.id in tweet['liked_users'] else "false",
                'bookmarked_users': "true" if request.user.id in tweet['bookmarked_users'] else "false",
            }
        print(Tweet.objects.filter(Q(author__in=users_query) | Q(author__name=name)).prefetch_related(
            prefetch_likes, prefetch_replies))
        return Response(my_dict, status = status.HTTP_200_OK)
    except Exception as e:
        print("Exception occurred on line number:", e.__traceback__.tb_lineno)
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def user_follow(request):
    response = {"msg": "","data":{}}
    try:
        if not request.user.is_authenticated:
            response['msg'] = "User not authenticated"
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        user_id = request.user.id
        following_id = request.data['following_id']
        state = request.data.get('state','follow')
        if state == "follow":
            following_user = get_object_or_404(User, id=following_id)
            follower_user = get_object_or_404(User, id=user_id)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=following_id)
            user_rel.follower.add(follower_user)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=user_id)
            user_rel.following.add(following_user)

        else:
            following_user = get_object_or_404(User, id=following_id)
            follower_user = get_object_or_404(User, id=user_id)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=following_id)
            user_rel.follower.remove(follower_user)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=user_id)
            user_rel.following.remove(following_user)
        response['msg'] = f"{state} success"
        response['data'] = UserSerializer(follower_user).data
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['POST'])
def search_box(request):
    try:
        response = {"msg": "", "data": {}}
        search_value = request.data.get('search_field')
        hashtag_obj = Hashtag.objects.filter(name__icontains=search_value)
        user_names = User.objects.filter(name__icontains=search_value)

        tweets = Tweet.objects.filter(hashtag_links__hashtag__in=hashtag_obj).annotate(
                num_likes=Count('likes__user', distinct=True),
                num_comments=Count('tweet_replies', distinct=True),
            ).order_by("-time")
        response['data'] = {
        'related_tweets' : TweetSerializer(tweets, many=True,context={'request': request}).data,
        'related_users' : UserSerializer(user_names, many=True).data
        }
        print(response)
        return Response(response, status=status.HTTP_200_OK)

    except Exception as e:
        print(e)
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def tweet_detail(request):
    response = {'msg': ''}
    try: 
        if not request.user.is_authenticated:
            response['msg'] = "User not authenticated"
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        actions = {
            'delete_tweet': delete_tweet,
            'create_tweet': create_tweet,
            'delete_comment': delete_comment,
            'like': like,
            'tweet_bookmark' : tweet_bookmark
        }
        action = request.GET.get('action')
        if action in actions:
            try:
                retData = actions[action](request)
                response['msg'] = "success"
                if(retData) : response['data']=retData
            except Exception as e:
                response['msg'] = str(e)
        else:
            response['msg'] = f'invalid action: {action}'
        
        return Response(response, status=status.HTTP_200_OK)

    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

  
def delete_tweet(request):
    tweet_id = request.data.get('tweet_id')
    tweet = get_object_or_404(Tweet, tweet_id = tweet_id)
    # print(tweet.author.strip() != request.user.username.strip())
    # if(tweet.author != (request.user.name)) : return None
    tweet.delete()
    
def create_tweet(request):
    try:
        text = request.data.get('text')
        user = User.objects.get(id=request.user.id)
        keyword = keywords(text, words=1)
        new_tweet = Tweet.objects.create(author=user, text=text, tweet_type=keyword)
        words = text.split()
        for word in words:
            if word.startswith('#'):
                tweet_hashtag, created = TweetHashtag.objects.get_or_create(tweet=new_tweet)
                hashtag_name = word[1:]
                hashtag, created = Hashtag.objects.get_or_create(name=hashtag_name)
                tweet_hashtag.hashtag.add(hashtag)
        tweet_data = {
            "key": new_tweet.tweet_id,
            "text": new_tweet.text,
            "author": new_tweet.author.username,
            "time": new_tweet.time,
            "comments":"",
            "tweet_type": new_tweet.tweet_type,
            "likes": new_tweet.likes.count(),
            "bookmarked_users": "false",
            "liked_users": "false"
        }
        return tweet_data

    except Exception as e:
        print(e)
    return {"data":tweet_data, "msg": "created"}
    

        
def like(request):
    id_of = request.data.get('id_of')
    action = request.data.get('action')
    action_for = request.data.get('action_for')
    user_id = request.user.id
    print(id_of,action,action_for,user_id)
    if action_for == "reply":
        content_type = ContentType.objects.get_for_model(Reply)
        my_model_instance = Reply.objects.get(id=id_of)
    else:
        content_type = ContentType.objects.get_for_model(Tweet)
        my_model_instance = Tweet.objects.get(tweet_id=id_of)
    if action == "add":
        if user_id not in my_model_instance.liked_users:
            my_model_instance.liked_users.append(user_id)
            my_model_instance.save()          
        obj, created = UserTweetLikes.objects.get_or_create(
            user_id=user_id,
            content_type=content_type,
            object_id=id_of
        )
    else:
        if user_id in my_model_instance.liked_users:
            my_model_instance.liked_users.remove(user_id)
            my_model_instance.save()
        UserTweetLikes.objects.get(
            user_id = user_id,
            content_type = content_type,
            object_id = id_of
        ).delete()
        

   
    
    


def delete_comment(request):
    comment_id = request.data.get('comment_id')
    Reply.objects.get(id=comment_id).delete()

def tweet_bookmark(request):
    action = request.data.get('action')
    tweet_id = request.data.get('tweet_id')
    action_for = request.data.get('action_for')
    user_id = request.user.id
    print(action,tweet_id,action_for,user_id)
    if action_for=="reply":
        my_model_instance = Reply.objects.get(id=tweet_id)
    else:
        my_model_instance = Tweet.objects.get(tweet_id=tweet_id)
    print(my_model_instance)
    if action == "add":
        if user_id not in my_model_instance.bookmarked_users:
            
            my_model_instance.bookmarked_users.append(user_id)
            my_model_instance.save()
    else:
        if user_id in my_model_instance.bookmarked_users:
            print("remove")
            my_model_instance.bookmarked_users.remove(user_id)
            my_model_instance.save()

@api_view(['GET'])
def get_trending_tags(request):
    response = {"msg":""}
    try:
        last_24_hours = datetime.now() - timedelta(hours=24)
        queryset = Hashtag.objects.filter(time__gte=last_24_hours)\
                                .annotate(
                                    num_tweets=Count('tweet_links')
                                    )\
                                .order_by('-num_tweets')[:5]
        response["data"] = TrendingTagsSerializer(queryset, many=True).data
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET'])
def get_tags_tweet(request):
    response = {'msg': ''}
    try:
        hashtag_name = request.GET.get('hashtag_id')
        hashtag_obj = Hashtag.objects.filter(id__iregex=hashtag_name)
        related_tweets = Tweet.objects.filter(hashtag_links__hashtag__in=hashtag_obj).annotate(
            num_comments=Count('tweet_replies', distinct=True),
            num_likes=Count('likes', distinct=True),
        ).order_by("-time")
        response['related_tweets'] = TweetSerializer(related_tweets, many=True,context={'request': request}).data
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def tweets_for_anonymous(request, page=1, per_page=5):
    try:
        my_dict = {}
        offset = (page - 1) * per_page
        limit = per_page
        tweet_ids = list(Tweet.objects.values_list('tweet_id', flat=True))
        prefetch_likes = Prefetch('likes', queryset=UserTweetLikes.objects.all())
        prefetch_replies = Prefetch('tweet_replies', queryset=Reply.objects.filter(parent_reply=None))
        tweets_query = Tweet.objects.filter(tweet_id__in=tweet_ids).prefetch_related(
            prefetch_likes, prefetch_replies).annotate(
            num_comments=Count('tweet_replies', distinct=True, filter=Q(tweet_replies__parent_reply=None)),
            num_likes=Count('likes', distinct=True),
        ).order_by('-time').values(
            'text', 'author__name', 'tweet_id', 'num_comments', 'num_likes', 'time', 'author__id', 'bookmarked_users','liked_users'
        )[offset:offset + limit]
        # print(tweets_query)
        for tweet in tweets_query:
            my_dict[tweet['tweet_id']] = {
                'key': tweet['tweet_id'],
                'text': tweet['text'],
                'likes': tweet['num_likes'],
                'comments': tweet['num_comments'],
                'author': tweet['author__name'],
                'author_id': tweet['author__id'],
                'time': tweet['time'],
                'liked_users': True if request.user.id in tweet['liked_users'] else False,
                'bookmarked_users': True if request.user.id in tweet['bookmarked_users'] else False,
            }
        return my_dict
    except Exception as e :
        print(e)
        pass

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return super(DateTimeEncoder, self).default(obj)