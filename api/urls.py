from django.urls import path, include
from . import views

urlpatterns = [
    path('reply/',views.reply, name='reply'),
    path('user_follow/',views.user_follow, name='user_follow'),
    path('search_box/',views.search_box, name='search_box'),
    path('get_tweets/',views.get_tweets, name='get_tweets'),
    path('tweet_detail/',views.tweet_detail, name='tweet_detail'),
    path('get_trending_tags/',views.get_trending_tags, name='get_trending_tags'),
    path('get_tags_tweet/',views.get_tags_tweet, name='get_tags_tweet'),
]
