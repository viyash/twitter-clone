from django.contrib import admin
from .models import User, Tweet, Reply, Hashtag, UserTweetLikes, TweetHashtag, UserRelationship
# Register your models here

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = [field.name for field in User._meta.fields]

@admin.register(Tweet)
class TweetAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Tweet._meta.fields]

@admin.register(Reply)
class ReplyAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Reply._meta.fields]

@admin.register(Hashtag)
class HashtagAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Hashtag._meta.fields]

@admin.register(UserTweetLikes)
class UserTweetLikesAdmin(admin.ModelAdmin):
    list_display = [field.name for field in UserTweetLikes._meta.fields]

@admin.register(TweetHashtag)
class TweetHashtagAdmin(admin.ModelAdmin):
    list_display = [field.name for field in TweetHashtag._meta.fields]

@admin.register(UserRelationship)
class UserRelationshipAdmin(admin.ModelAdmin):
    list_display = [field.name for field in UserRelationship._meta.fields]

