from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status


from django.db.models import Count, Q, Prefetch
from django.core import serializers
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignupForm
from .models import Reply, User, UserRelationship, Tweet, TweetHashtag, Hashtag, UserTweetLikes
from .serializer import TweetSerializer
from django.contrib.contenttypes.models import ContentType
from datetime import datetime, timedelta


import json
from .views import DateTimeEncoder



@api_view(['POST','GET'])
def login_view(request):
    response = {"msg": "","data":{}}
    try:
        if request.user.is_authenticated:
            return Response("logged in already", status = status.HTTP_200_OK)
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(request, username=username, password=password)
                if user is not None:
                    login(request._request, user)
                    response['msg'] = "logged in successfully!" 
                    return Response(response, status = status.HTTP_200_OK)
                response['msg'] = "unauthenticated user!" 
                return Response(response, status = status.HTTP_401_UNAUTHORIZED)
        else:
            form = LoginForm()
            return render(request, 'login.html', {'form': form})
    except Exception as e:
        print(e)
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST','GET'])
def signup(request):
    response = {"msg": "","data":{}}
    try:
        if request.method == "POST":
            form = SignupForm(request.POST)
            if form.is_valid():
                user = form.save()
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=password)
                login(request._request)
                response['msg'] = "signed in successfully!" 
                return Response(response, status = status.HTTP_200_OK)
        else:
            form = SignupForm()
            return render(request, 'signup.html', {'form': form})
    except Exception as e:
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def logout_view(request):
    response = {"msg": "", "data": {}}
    try:
        if request.user.is_authenticated:
            logout(request._request)
            response['msg'] = "logged out successfully"
        else:
            response['msg'] = "user not authenticated"
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


{
"tweet_id":"11",
"text":"shit",
"parent_reply":""
}
@api_view(['GET', 'POST'])
def reply(request):
    response = {"msg": "","data":{}}
    try:
        if request.method == 'GET':
            tweet_id = request.GET.get('tweet_id')
            reply_query = Reply.objects.filter(tweet__tweet_id=tweet_id, parent_reply=None).annotate(
                num_likes=Count('likes', distinct=True),
                num_comments=Count('parent_replies', distinct=True),
            )
            serialized_reply = json.loads(serializers.serialize('json', reply_query))
            response['msg'] = "success"
            response['data'] = serialized_reply
            return Response(serialized_reply, status = status.HTTP_200_OK)
        elif request.method == 'POST':
            if not request.user.is_authenticated:
                response['msg'] = "User not authenticated"
                return Response(response, status=status.HTTP_401_UNAUTHORIZED)
            else:
                user_id = request.user.id
                tweet_id = request.data['tweet_id']
                text = request.data['text']
                parent_reply = request.data['parent_reply']
                parent_reply = Reply.objects.get(id=parent_reply) if parent_reply else None
                reply_obj = Reply.objects.create(
                    author_id =user_id,
                    tweet_id=tweet_id,
                    text=text,
                    parent_reply=parent_reply,
                )
                serialized_reply = json.loads(serializers.serialize('json', [reply_obj]))
                response['data'] = serialized_reply
                response['msg'] = "success"
                return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


{
    "following_id":"3",
    "state" :""
}

@api_view(['POST'])
def user_follow(request):
    response = {"msg": "","data":{}}
    try:
        if not request.user.is_authenticated:
            response['msg'] = "User not authenticated"
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        user_id = request.user.id
        following_id = request.data['following_id']
        state = request.data.get('state','follow')
        if state == "follow":
            following_user = get_object_or_404(User, id=following_id)
            follower_user = get_object_or_404(User, id=user_id)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=following_id)
            user_rel.follower.add(follower_user)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=user_id)
            user_rel.following.add(following_user)

        else:
            following_user = get_object_or_404(User, id=following_id)
            follower_user = get_object_or_404(User, id=user_id)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=following_id)
            user_rel.follower.remove(follower_user)
            user_rel, created = UserRelationship.objects.get_or_create(user_id_id=user_id)
            user_rel.following.remove(following_user)
        response['msg'] = f"{state} success"
        response['data'] = json.loads(serializers.serialize('json', [follower_user]))
        return Response(response, status=status.HTTP_200_OK)

    except Exception as e:
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


{
"search_field":"v"
}

@api_view(['POST'])
def search_box(request):
    try:
        response = {"msg": "", "data": {}}
        search_value = request.data.get('search_field')
        if not search_value : return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        hashtag_obj = Hashtag.objects.filter(name__iregex=search_value)
        user_name = User.objects.filter(name__iregex = search_value)
        hashtag_obj = Tweet.objects.filter(hashtag_links__hashtag__in=hashtag_obj).order_by("-time")
        response['data']['related_tweets'] = json.loads(serializers.serialize('json', hashtag_obj))
        response['data']['related_users'] = json.loads(serializers.serialize('json', user_name))
        return Response(response, status=status.HTTP_200_OK)

    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET','POST'])
def get_tweets(request, page=1, per_page=5):
    try:
        
        my_dict = {}
        if not request.user.is_anonymous:
            name = request.user.name
            users_query = UserRelationship.objects.get(user_id__name=name).following.all()
        else:
            return tweets_for_anononymus(request, page=1, per_page=5)

        #pagination
        offset = (page - 1) * per_page
        limit = per_page

        prefetch_likes = Prefetch('likes', queryset=UserTweetLikes.objects.filter(user__in=users_query))
        prefetch_replies = Prefetch('tweet_replies', queryset=Reply.objects.filter(parent_reply=None))
        tweets_query = Tweet.objects.filter(Q(author__in=users_query) | Q(author__name=name)).prefetch_related(
            prefetch_likes, prefetch_replies).annotate(
            num_comments=Count('tweet_replies', distinct=True, filter=Q(tweet_replies__parent_reply=None)),
            num_likes=Count('likes', distinct=True, filter=Q(likes__user__in=users_query)),
        ).order_by('-time')[offset:offset + limit].values(
            'text', 'author__name', 'tweet_id', 'num_comments', 'num_likes', 'time', 'author__id'
        )

        for tweet in tweets_query:
            my_dict[tweet['tweet_id']] = {
                'text': tweet['text'],
                'likes': tweet['num_likes'],
                'comments': tweet['num_comments'],
                'author': tweet['author__name'],
                'author_id': tweet['author__id'],
                'time': tweet['time'],
            }
    
        return Response(my_dict, status = status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



{
"tweet_id":1
}
@api_view(['POST'])
def tweet_detail(request):
    response = {'msg': ''}
    try: 
        if not request.user.is_authenticated:
            response['msg'] = "User not authenticated"
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        actions = {
            'delete_tweet': delete_tweet,
            'create_tweet': create_tweet,
            'delete_like': delete_like,
            'delete_comment': delete_comment,
            'add_like':add_like
        }
        action = request.GET.get('action')
        if action in actions:
            try:
                actions[action](request)
                response['msg'] = f'successfully {action}'
            except Exception as e:
                response['msg'] = str(e)
        else:
            response['msg'] = f'invalid action: {action}'
        
        return Response(response, status=status.HTTP_200_OK)

    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

  
def delete_tweet(request):
    tweet_id = request.data.get('tweet_id')
    tweet = get_object_or_404(Tweet, tweet_id = tweet_id)
    tweet.delete()
    
def create_tweet(request):
    text = request.data.get('text')
    user = User.objects.get(id=request.user.id)
    new_tweet = Tweet.objects.create(author=user, text=text)
    words = text.split()
    for word in words:
        if word.startswith('#'):
            tweet_hashtag, created = TweetHashtag.objects.get_or_create(tweet=new_tweet)
            hashtag_name = word[1:]
            hashtag, created = Hashtag.objects.get_or_create(name=hashtag_name)
            tweet_hashtag.hashtag.add(hashtag)
    
def delete_like(request):
    like_id = request.data.get('like_id')
    UserTweetLikes.objects.get(id=like_id).delete()

def add_like(request):
    id_of = request.data.get('id')
    action_for = request.data.get('action_for')
    if action_for == "reply":
        content_type = ContentType.objects.get_for_model(Reply)
        UserTweetLikes.objects.create(
            user_id = request.user.id,
            content_type = content_type,
            object_id = id_of
            )
    else:
        content_type = ContentType.objects.get_for_model(Tweet)
        UserTweetLikes.objects.create(
            user_id = request.user.id,
            content_type = content_type,
            object_id = id_of
            )

def delete_comment(request):
    comment_id = request.data.get('comment_id')
    Reply.objects.get(id=comment_id).delete()


@api_view(['GET'])
def get_nested_reply(request):
    try:
        msg_id = request.GET.get('msg_id')
        offset = (int(request.GET.get('page', 1)) - 1) * int(request.GET.get('per_page', 5))
        limit = int(request.GET.get('per_page', 5))
        reply_query = Reply.objects.filter(parent_reply_id=msg_id).annotate(
            num_likes=Count('likes', distinct=True),
            num_comments=Count('parent_replies', distinct=True),
        ).values()[offset:offset+limit]
        return Response(reply_query, status=status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def get_trending_tags(request):
    response = {"msg":""}
    try:
        last_24_hours = datetime.now() - timedelta(hours=24)
        queryset = Hashtag.objects.filter(time__gte=last_24_hours)\
                                .annotate(num_tweets=Count('tweet_links'))\
                                .order_by('-num_tweets')\
                                .values()
        response["data"] = json.dumps(list(queryset),cls=DateTimeEncoder)
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
def get_tags_tweet(request):
    response = {'msg': ''}
    try:
        hashtag_name = request.GET.get('hashtag_name')
        hashtag_obj = Hashtag.objects.filter(name__iregex=hashtag_name)
        related_tweets = Tweet.objects.filter(hashtag_links__hashtag__in=hashtag_obj).annotate(
            num_comments=Count('tweet_replies', distinct=True),
            num_likes=Count('likes', distinct=True),
        ).order_by("-time").values()
        response['related_tweets'] = json.dumps(list(related_tweets),cls = DateTimeEncoder)
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        response['msg'] = "Something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
from rest_framework import serializers
from .models import (   
                    Tweet, 
                    Reply, 
                    UserRelationship, 
                    User,
                    Hashtag
                    )

class ReplySerializer(serializers.ModelSerializer):
    num_likes = serializers.IntegerField()
    num_comments = serializers.IntegerField()

    class Meta:
        model = Reply
        fields = "__all__"

class TweetSerializer(serializers.ModelSerializer):
    author_username = serializers.CharField(source='author.username')
    num_likes = serializers.IntegerField()
    num_comments = serializers.IntegerField()

    class Meta:
        model = Tweet
        fields = ['tweet_id', 'author_id', 'author_username', 'text', 'num_likes', 'num_comments', 'time']


class ReplyCreateSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Reply
        fields = ['author', 'tweet', 'text', 'parent_reply']

    def create(self, validated_data):
        tweet = validated_data.get('tweet')
        text = validated_data.get('text')
        parent_reply = validated_data.get('parent_reply')
        author = self.context['request'].user
        reply = Reply.objects.create(
            author=author,
            tweet=tweet,
            text=text,
            parent_reply=parent_reply
        )

        return reply


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class SearchResponseSerializer(serializers.Serializer):
    tweets = serializers.SerializerMethodField()
    users = serializers.SerializerMethodField()

    def get_tweets(self, obj):
        print(obj)
        tweets = obj.get('related_tweets')
        return searchHashSerializer(tweets, many=True).data

    def get_users(self, obj):
        search_value = self.context['request'].data.get('search_field')
        if not search_value:
            return []

        users = User.objects.filter(name__icontains=search_value)
        return searchUserSerializer(users, many=True).data
    
    def create(self, validated_data):
        return validated_data





class UserRelationshipSerializer(serializers.ModelSerializer):
    user_id = UserSerializer(read_only=True)
    follower = UserSerializer(many=True, read_only=True)
    following = UserSerializer(many=True, read_only=True)

    class Meta:
        model = UserRelationship
        fields = ['id', 'user_id', 'follower', 'following']