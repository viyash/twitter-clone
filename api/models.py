from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    name = models.CharField(max_length=255)
    age = models.IntegerField(null=True)
    gender = models.CharField(max_length=50)
    occupation = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    is_twitter_user = models.BooleanField(default=False)

    # def save(self, *args, **kwargs):
    #     created = not self.pk
    #     super().save(*args, **kwargs)
    #     if created:
    #         UserRelationship.objects.create(user_id=self)

    def __str__(self):
        return self.username



class UserTweetLikes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return f"{self.user} - {self.content_object}"

class Tweet(models.Model):
    tweet_id = models.AutoField(primary_key=True)
    text = models.CharField(max_length=280)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tweets')
    time = models.DateTimeField(auto_now_add=True)
    tweet_type = models.CharField(max_length=10)
    likes = GenericRelation(UserTweetLikes)
    bookmarked_users = ArrayField(models.IntegerField(), default=list)
    liked_users = ArrayField(models.IntegerField(), default=list)
    
    def __str__(self):
        return f"{self.tweet_id} - {self.text} - {self.author}"

class Reply(models.Model):
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE, related_name='tweet_replies')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author_replies')
    text = models.CharField(max_length=280)
    time = models.DateTimeField(auto_now_add=True)
    parent_reply = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='parent_replies')
    likes = GenericRelation(UserTweetLikes)
    bookmarked_users = ArrayField(models.IntegerField(), default=list)
    liked_users = ArrayField(models.IntegerField(), default=list)

    def __str__(self):
        return f"replyid-{self.id} - tweetid-{self.tweet} - {self.author}"

class Hashtag(models.Model):
    name = models.CharField(max_length=140)
    time = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f"{self.name} - {self.time}"

class TweetHashtag(models.Model):
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE, related_name='hashtag_links')
    hashtag = models.ManyToManyField(Hashtag, related_name='tweet_links')
    
    def __str__(self):
        return f"{self.tweet} - {self.hashtag}"

class UserRelationship(models.Model):
   
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='main_user')
    follower = models.ManyToManyField(User, related_name='following')
    following = models.ManyToManyField(User, related_name='followers')

    def __str__(self):
        return f"{self.follower} - {self.following}"
