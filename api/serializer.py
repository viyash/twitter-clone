from rest_framework import serializers
from .models import (   
                    Tweet, 
                    Reply, 
                    UserRelationship, 
                    User,
                    Hashtag
                    )

class ReplySerializer(serializers.ModelSerializer):
    author__name = serializers.CharField()
    num_likes = serializers.IntegerField()
    num_comments = serializers.IntegerField()
    bookmarked_users = serializers.ListField(child=serializers.IntegerField())
    liked_users = serializers.ListField(child=serializers.IntegerField())
    author__id = serializers.IntegerField()

    class Meta:
        model = Reply
        fields = ['id','text', 'author__name','tweet_id','author__id','num_comments', 'num_likes', 'time','liked_users','bookmarked_users']
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)

        user_id = self.context['request'].user.id
        bookmarked_users = representation['bookmarked_users']
        liked_users = representation['liked_users']

        representation['bookmarked_users'] = 'true' if user_id in bookmarked_users else 'false'
        representation['liked_users'] = 'true' if user_id in liked_users else 'false'

        return representation


class Reply2Serializer(serializers.ModelSerializer):
    author_name = serializers.CharField()
    class Meta:
        model = Reply
        fields = ['author_name','author','id','parent_reply','text','time','tweet']


class TweetSerializer(serializers.ModelSerializer):
    author_username = serializers.CharField(source='author.username')
    num_likes = serializers.IntegerField()
    num_comments = serializers.IntegerField()
    bookmarked_users = serializers.ListField(child=serializers.IntegerField())
    liked_users = serializers.ListField(child=serializers.IntegerField())

    class Meta:
        model = Tweet
        fields = ['tweet_id', 'author_id', 'author_username', 'text', 'num_likes', 'num_comments', 'time','bookmarked_users','liked_users']

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        user_id = self.context['request'].user.id
        bookmarked_users = representation['bookmarked_users']
        liked_users = representation['liked_users']

        representation['bookmarked_users'] = 'true' if user_id in bookmarked_users else 'false'
        representation['liked_users'] = 'true' if user_id in liked_users else 'false'

        return representation



class TrendingTagsSerializer(serializers.ModelSerializer):
    num_tweets = serializers.IntegerField()
    class Meta:
        model = Hashtag
        fields = "__all__"



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']



class UserRelationshipSerializer(serializers.ModelSerializer):
    user_id = UserSerializer(read_only=True)
    follower = UserSerializer(many=True, read_only=True)
    following = UserSerializer(many=True, read_only=True)

    class Meta:
        model = UserRelationship
        fields = ['id', 'user_id', 'follower', 'following']