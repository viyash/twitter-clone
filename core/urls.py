from django.urls import path, include
from . import views
from django.views.generic.base import RedirectView


app_name = 'core'
urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),



    path('index/', views.index, name='index'),
    path('check/', views.check, name='check'),
    path(
        "favicon.ico",
        RedirectView.as_view(url="/static/icons/favicon.ico", permanent=True),
    ),
]
