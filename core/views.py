from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignupForm
from api.models import Reply, User, UserRelationship, Tweet, TweetHashtag, Hashtag, UserTweetLikes
from django.core import serializers
from django.urls import reverse
from django.shortcuts import redirect, HttpResponse, HttpResponseRedirect

import requests
import json
from api.views import get_tweets
from twitter.settings import URL
from api.serializer  import UserRelationshipSerializer, UserSerializer

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import LoginForm
from django.contrib import messages

def login_view(request):
    if request.user.is_authenticated:
        return redirect('core:index')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('core:index')
        error_msg = "Invalid username or password."
        return render(request, 'html/login.html', {'form': form, 'error_msg': error_msg})
    else:
        form = LoginForm()
        return render(request, 'html/login.html', {'form': form})

@api_view(['POST','GET'])
def signup(request):
    response = {"msg": "","data":{}}
    try:
        if request.method == "POST":
            form = SignupForm(request.POST)
            if form.is_valid():
                user = form.save()
                # username = form.cleaned_data.get('username')
                # password = form.cleaned_data.get('password1')
                # user = authenticate(username=username, password=password)
                # login(request._request, user)
                success_msg = "Please Login In To Continue." 
                return redirect(reverse('core:login') + f"?success_msg={success_msg}")
            else:
                error_string = ""
                for field in form:
                    for error in field.errors:
                        error_string += f"{field.label}: {error}\n"
        else:
            form = SignupForm()
            return render(request, 'html/signup.html', {'form': form})
        return render(request, 'html/signup.html', {'form': form, "error_msg":error_string})

    except Exception as e:
        print(e)
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def logout_view(request):
    response = {"msg": "", "data": {}}
    try:
        if request.user.is_authenticated:
            logout(request._request)
            response['msg'] = "logged out successfully"
            return redirect(reverse('core:index'))
        else:
            response['msg'] = "user not authenticated"
        return Response(response, status=status.HTTP_200_OK)
    except Exception as e:
        response['msg'] = "something went wrong!"
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



def get_followers(userid=3):
    queryset = UserRelationship.objects.filter(user_id=userid)
    return Response(UserRelationshipSerializer(queryset, many=True).data, status=status.HTTP_200_OK)


def profileData(userid):
    queryset = User.objects.get(id=userid)
    relationship = UserRelationship.objects.filter(user_id=queryset)
    data = {
        "user": UserSerializer(queryset, many=False).data,
    }

    if len(relationship) > 0:
        data["relations"] = json.loads(serializers.serialize('json',relationship))[0]['fields']
    else:
        data["relations"] = None  

    return data




import requests

def index(request, profile=None):
    session_id = request.COOKIES.get('sessionid')
    headers = {'Cookie': f'sessionid={session_id};'}
    TweetsData = requests.get(f'{URL}/api/get_tweets/', headers=headers)
    trendingTags = requests.get(f'{URL}/api/get_trending_tags/')
    data = {
        'TweetsData': TweetsData.json(),
        'trendingTags': trendingTags.json()['data'],
    }
    if not request.user.is_anonymous:
        profile = profileData(request.user.id)
        if profile is not None:
            data['profileData'] = profile
            if profile.get('relations'):
                data['following_user'] = profile.get('relations').get('following', False)
    return render(request, 'html/index.html', data)


def check(request):
    while True:
        query = User.objects.all()
        for i in query:
            User.objects.get(id=i.id).save()
    return render(request, 'html/index.html', data)