from django import forms
from django.contrib.auth.forms import UserCreationForm
from api.models import User

class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    password = forms.CharField(label='Password', max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))


class SignupForm(UserCreationForm):
    username = forms.CharField(label='Username',max_length=200, help_text='Required',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    email = forms.EmailField(label='email',max_length=200, help_text='Required',widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    name = forms.CharField(label='Name',max_length=200, help_text='Required', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}))
    age = forms.IntegerField(label='Age',help_text='Required',widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Age'}))
    gender = forms.CharField(label='Gender',max_length=50, help_text='Required',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Gender'}))
    occupation = forms.CharField(label='Occupation',max_length=200, help_text='Required',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Occupation'}))
    location = forms.CharField(label='location',max_length=200, help_text='Required',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Location'}))
    password1 = forms.CharField(label='Password', max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    password2 = forms.CharField(label='Confirm Password', max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirm Password'}))

    class Meta:
        model = User
        fields = ('username','email', 'name', 'age', 'gender', 'occupation', 'location', 'password1', 'password2')

