from django.shortcuts import render, get_object_or_404, redirect
from .models import User, Tweet, UserRelationship, UserTweetLikes, Reply, Hashtag, TweetHashtag
from django.http import HttpResponse
from django.db.models import Q, Count, Prefetch, F, Case, TextField, When, Value, CharField
from django.contrib.contenttypes.models import ContentType
from collections import defaultdict
from datetime import datetime
import json
from datetime import datetime, timezone, timedelta
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignupForm
from django.contrib.auth.decorators import login_required
from django.urls import reverse


def time_since(timestamp):
    if timestamp.tzinfo is None:
        timestamp = timestamp.replace(tzinfo=timezone.utc)
    now = datetime.now(timezone.utc)
    delta = now - timestamp
    if delta < timedelta(minutes=1):
        return 'just now'
    elif delta < timedelta(hours=1):
        minutes = delta.seconds // 60
        return f'{minutes} minute{"s" if minutes != 1 else ""} ago'
    elif delta < timedelta(days=1):
        hours = delta.seconds // 3600
        return f'{hours} hour{"s" if hours != 1 else ""} ago'
    else:
        days = delta.days
        return f'{days} day{"s" if days != 1 else ""} ago'




class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return super(DateTimeEncoder, self).default(obj)
    

def get_Tweets_likes(users_query, name):
    prefetch_likes = Prefetch('likes', queryset=UserTweetLikes.objects.filter(user__in=users_query))
    prefetch_replies = Prefetch('tweet_replies', queryset=Reply.objects.filter(parent_reply=None))
    Tweets_query = Tweet.objects.filter(Q(author__in=users_query) | Q(author__name=name)).prefetch_related(
        prefetch_likes, prefetch_replies).annotate(
        num_comments=Count('tweet_replies', distinct=True,filter=Q(tweet_replies__parent_reply=None)),
        num_likes=Count('likes', distinct=True, filter=Q(tweet_replies__parent_reply=None)),
    ).values('text', 'author__name', 'tweet_id', 'num_comments', 'num_likes','time')
    return Tweets_query


def get_tweets(request, name, page=1, per_page=5):
    my_dict = defaultdict(int)
    users_query = UserRelationship.objects.get(user_id__name=name).following.all()
    offset = (page - 1) * per_page
    limit = per_page
    Tweets_query = get_Tweets_likes(users_query, name)[offset:offset+limit]
    my_dict = {  tweet['tweet_id']: {'text': tweet['text'], 
                'likes': tweet['num_likes'], 
                'comments': tweet['num_comments'],
                'author':tweet['author__name'],
                'time':time_since(tweet['time'])
                } for tweet in Tweets_query }

    return HttpResponse(json.dumps(my_dict, cls=DateTimeEncoder))



def get_reply(request):
    tweet_id = request.GET.get('tweet_id')
    reply_query = Reply.objects.filter(tweet__tweet_id=tweet_id, parent_reply=None).annotate(
        num_likes=Count('likes', distinct=True),
        num_comments=Count('parent_replies', distinct=True),
    ).values()
    return HttpResponse(json.dumps(list(reply_query), cls=DateTimeEncoder))



def get_nested_reply(request, page=1, per_page=5):
    msg_id = request.GET.get('msg_id')
    offset = (page - 1) * per_page
    limit = per_page
    reply_query = Reply.objects.filter(parent_reply_id=msg_id).annotate(
        num_likes=Count('likes', distinct=True),
        num_comments=Count('parent_replies', distinct=True),
    ).values()[offset:offset+limit]
    return HttpResponse(json.dumps(list(reply_query), cls=DateTimeEncoder))


def get_trending_tags():
    # Calculate the datetime 24 hours ago
    last_24_hours = datetime.now() - timedelta(hours=24)
    queryset = Hashtag.objects.filter(time__gte=last_24_hours)\
                               .annotate(num_tweets=Count('tweet_links'))\
                               .values()\
                               .order_by('-num_tweets')
    return json.dumps(list(queryset), cls=DateTimeEncoder)



def get_tags_tweet(request):
    returnParams = {"status":0, "params":{}}
    hashtag_name = request.GET.get('hashtag_name')
    hashtag_obj = Hashtag.objects.filter(name__iregex=hashtag_name)
    returnParams['params']['related_tweets'] = list(Tweet.objects.filter(hashtag_links__hashtag__in=hashtag_obj).values().order_by("-time"))
    return HttpResponse(json.dumps(returnParams, cls=DateTimeEncoder))


def search_box(request):
    #http://localhost:8000/search_box?search_field='dum'
    returnParams = {"status":0, "params":{}}
    hashtag_name = request.GET.get('search_field')
    hashtag_obj = Hashtag.objects.filter(name__iregex=hashtag_name)
    returnParams['params']['related_tweets'] = list(Tweet.objects.filter(hashtag_links__hashtag__in=hashtag_obj).values().order_by("-time"))
    returnParams['params']['related_users'] = list(User.objects.filter(name__iregex=hashtag_name).values())
    return HttpResponse(json.dumps(returnParams, cls=DateTimeEncoder))

    
def user_following(request,following_id, user_id):
    returnParams = {"status":0, "params":{}}
    following_user = User.objects.get(id=following_id)
    follower_user = User.objects.get(id=user_id)
    user_rel, created = UserRelationship.objects.get_or_create(user_id_id=following_id)
    user_rel.follower.add(follower_user)
    user_rel, created = UserRelationship.objects.get_or_create(user_id_id=user_id)
    user_rel.following.add(following_user)
    return HttpResponse(json.dumps(returnParams, cls=DateTimeEncoder))


def user_unfollow(request,following_id, user_id):
    returnParams = {"status":0, "params":{}}
    following_user = User.objects.get(id=following_id)
    follower_user = User.objects.get(id=user_id)
    user_rel, created = UserRelationship.objects.get_or_create(user_id_id=following_id)
    user_rel.follower.remove(follower_user)
    user_rel, created = UserRelationship.objects.get_or_create(user_id_id=user_id)
    user_rel.following.remove(following_user)
    return HttpResponse(json.dumps(returnParams, cls=DateTimeEncoder))

@login_required
def HomePage(request, name=None):
    returnParams = {'status':0,'params':{}}
    if name:
        TweetsData = json.loads(get_tweets(request, name).content, cls=DateTimeEncoder)
        TrendingData = get_trending_tags()
        profileData = User.objects.filter(name = name).values()
        returnParams['params']['data'] = TweetsData
        returnParams['params']['trending'] = TrendingData 
        returnParams['params']['profileData'] = json.dumps(list(profileData))
    else:
        TrendingData = get_trending_tags()
        queryset =  Tweet.objects.all().order_by('-time').values()
        returnParams['params']['data'] = json.dumps(list(queryset), cls=DateTimeEncoder)
        returnParams['params']['trending'] = TrendingData 
    return render(request, 'index.html',returnParams)

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(f'/HomePage/{username}')
            else:
                form.add_error(None, "Invalid username or password.")
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect(f'/HomePage/{username}')
    else:
        form = SignupForm()
    return render(request, 'signup.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('/HomePage/')



def tweet_detail(request, tweet_id):
    returnParams = {'status':0,'params':{},'msg':''}
   
    actions = {
        'delete_tweet': delete_tweet,
        'create_tweet': create_tweet,
        'delete_like': delete_like,
        'delete_comment': delete_comment
    }
    
    action = request.GET.get('action')
    if action in actions:
        try:
            actions[action](request)
            returnParams['msg'] = f'successfully {action}'
        except Exception as e:
            returnParams['msg'] = str(e)
    else:
        returnParams['msg'] = f'invalid action: {action}'
    
    return HttpResponse(json.dumps(returnParams, cls=DateTimeEncoder))
    
def delete_tweet(request):
    tweet_id = request.GET.get('tweet_id')
    tweet = get_object_or_404(Tweet,pk=tweet_id)
    tweet.delete()
    
def create_tweet(request):
    text = request.GET.get('text')
    user = User.objects.get(id=request.GET.get('user'))
    new_tweet = Tweet.objects.create(author=user, text=text)
    words = text.split()
    hashtags = []
    for word in words:
        if word.startswith('#'):
            tweet_hashtag, created = TweetHashtag.objects.get_or_create(tweet=new_tweet)
            hashtag_name = word[1:]
            hashtag, created = Hashtag.objects.get_or_create(name=hashtag_name)
            tweet_hashtag.hashtag.add(hashtag)
    
def delete_like(request):
    like_id = request.GET.get('like_id')
    UserTweetLikes.objects.get(id=like_id).delete()
    
def delete_comment(request):
    comment_id = request.GET.get('comment_id')
    Reply.objects.get(id=comment_id).delete()



